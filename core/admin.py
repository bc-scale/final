from django.contrib import admin
from . import models


@admin.register(models.Question)
class QuestionAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'user',
        '__str__',
        'slug',
        'created',
        'last_edit',
    )


@admin.register(models.Answer)
class AnswerAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        '__str__',
        'created',
        'last_edit',
    )