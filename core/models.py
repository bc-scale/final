from django.db import models
from django.contrib.auth import get_user_model
from django.utils.text import slugify


USER = get_user_model()

class Question(models.Model):
    user = models.ForeignKey(USER, on_delete=models.CASCADE, related_name='questions')
    title = models.CharField(max_length=200)
    slug = models.SlugField(max_length=210, unique=True)
    body = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    last_edit = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'question'
        verbose_name_plural = 'questions'

    def __str__(self):
        return f'{self.title}'

    def generate_slug(self, title):
        return slugify(title, allow_unicode=True)

    def save(self, *args, **kwargs):
        self.slug = self.generate_slug(self.title)
        return super().save(*args, **kwargs)


class Answer(models.Model):
    user = models.ForeignKey(USER, on_delete=models.CASCADE, related_name='answers')
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='answers')
    body = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    last_edit = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'answer'
        verbose_name_plural = 'answers'

    def __str__(self):
        return f'{self.user}-{self.question}'