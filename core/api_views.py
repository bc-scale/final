from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from permissions.api_permissions import IsOwnerOrReadonly, AdminGroupMixin
from . import api_serializers
from . import models


class QuestionListView(APIView):
    def get(self, request):
        question = models.Question.objects.all()
        srz_data = api_serializers.QuestionSerializer(instance=question, many=True).data
        return Response(srz_data, status=status.HTTP_200_OK)


class QuestionRetrieveView(APIView):
    permission_classes = [IsOwnerOrReadonly,]
    def get(self, request, pk):
        question = models.Question.objects.get(pk=pk)
        self.check_object_permissions(request, question)
        srz_data = api_serializers.QuestionSerializer(instance=question).data
        return Response(srz_data, status=status.HTTP_200_OK)


class QuestionCreateView(APIView):
    def post(self, request):
        """for validate data, send value to serializer"""
        data = api_serializers.QuestionSerializer(data=request.data)
        if data.is_valid():
            data.save()
            return Response(data.data, status=status.HTTP_201_CREATED)
        return Response(data.errors, status=status.HTTP_400_BAD_REQUEST)


class QuestionUpdateView(APIView):
    permission_classes = [IsOwnerOrReadonly,]
    def put(self, request, pk):
        """
            get instance and send more recived data to serializer for validation and update to instance
            may be data come to serializer is partial, so set partail True
        """
        question = models.Question.objects.get(pk=pk)
        self.check_object_permissions(request, question)
        srz_data = api_serializers.QuestionSerializer(
            instance=question,
            data=request.data,
            partial=True,
            )
        if srz_data.is_valid():
            srz_data.save()
            return Response(srz_data.data, status=status.HTTP_200_OK)
        return Response(srz_data.errors, status=status.HTTP_400_BAD_REQUEST)


class QuestionDeleteView(APIView):
    permission_classes = [AdminGroupMixin,]
    def delete(self, request, pk):
        question = models.Question.objects.get(pk=pk)
        self.check_object_permissions(request, question)
        question.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class AnswerListView(APIView):
    def get(self, request, pk):
        answer = models.Answer.objects.filter(question__id=pk)
        srz_data = api_serializers.AnswerSerializer(instance=answer, many=True).data
        return Response(srz_data, status=status.HTTP_200_OK)


class AnswerCreateView(APIView):
    def post(self, request, pk):
        data = api_serializers.AnswerSerializer(data=request.data)
        if data.is_valid():
            data.save()
            return Response(data.data, status=status.HTTP_201_CREATED)
        return Response(data.errors, status=status.HTTP_400_BAD_REQUEST)


class AnswerUpdateView(APIView):
    permission_classes = [IsOwnerOrReadonly,]
    def put(self, request, pk):
        answer = models.Answer.objects.get(pk=pk)
        self.check_object_permissions(request, answer)
        srz_data = api_serializers.AnswerSerializer(instance=answer, data=request.data, partial=True)
        if srz_data.is_valid():
            srz_data.save()
            return Response(srz_data.data, status=status.HTTP_200_OK)
        return Response(srz_data.errors, status=status.HTTP_400_BAD_REQUEST)


class AnswerDeleteView(APIView):
    permission_classes = [AdminGroupMixin,]
    def delete(self, request, pk):
        answer = models.Answer.objects.get(pk=pk)
        self.check_object_permissions(request, answer)
        answer.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
