from rest_framework.permissions import BasePermission, SAFE_METHODS


class IsOwnerOrReadonly(BasePermission):
    message = 'permission denied, you٬re not the owner'

    def has_permission(self, request, view):
        """check permissions outside of view"""
        return request.user.is_authenticated and request.user

    def has_object_permission(self, request, view, obj):
        """
            check permissions in view.
            for active this method, in view call self.check_object_permissions(request, obj)
        """
        if request.method in SAFE_METHODS:
            return True
        return obj.user == request.user or request.user.is_superuser


class AdminGroupMixin(IsOwnerOrReadonly):
    """
        inherited from IsOwnerOrReadonly because that inherited from BasePermission
        and overwrite has_object_permission method
    """
    message = 'permission denied, you have not access'

    def has_permission(self, request, view):
        """check permissions outside of view"""
        return request.user.groups.filter(name='admin').exists()
