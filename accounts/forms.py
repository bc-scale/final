from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import CustomUser


class CustomUserCreationForm(UserCreationForm):
    """this form for custom user"""
    class Meta:
        model = CustomUser
        fields = ('email', 'is_superuser',)


class CustomUserChangeForm(UserChangeForm):
    """this form for custom user"""
    class Meta:
        model = CustomUser
        fields = (
            'email',
            'is_superuser',
            )


class UserRegistrationForm(forms.Form):
    username = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control'}
        )
    )
    email = forms.EmailField(
        widget=forms.EmailInput(
            attrs={'class': 'form-control'}
        )
    )
    password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={'class': 'form-control'}
        )
    )



class UserLoginForm(forms.Form):
    """this is custom form for LoginView"""
    username = forms.CharField(
        widget=forms.TextInput(
            attrs={'class':'form-control'}
            )
        )
    password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={'class':'form-control'}
            )
        )
        

class UserAvatarForm(forms.Form):
    """update user avatar"""
    avatar = forms.ImageField(
            widget=forms.FileInput(
                attrs={'class': 'btn btn-info'}
                )
            )