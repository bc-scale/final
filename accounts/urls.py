from django.urls import path, include
from . import views


app_name = 'accounts'
urlpatterns = [
    path('register/', views.UserRegister.as_view(), name='register'),
    path('login/', views.UserLogin.as_view(), name='login'),
    path('logout/', views.UserLogout.as_view(), name='logout'),
    path('dashboard/', views.UserDashboard.as_view(), name='dashboard'),
    path('api/', include('accounts.api_urls', namespace='jwt')),
]