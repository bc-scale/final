from django.test import SimpleTestCase
from django.urls import reverse, resolve
from accounts import views


class TestUrl(SimpleTestCase):
    def test_register(self):
        url = reverse('accounts:register')
        self.assertEqual(resolve(url).func.view_class, views.UserRegister)

    def test_login(self):
        url = reverse('accounts:login')
        self.assertEqual(resolve(url).func.view_class, views.UserLogin)

    def test_logout(self):
        url = reverse('accounts:logout')
        self.assertEqual(resolve(url).func.view_class, views.UserLogout)

    def test_dashboard(self):
        url = reverse('accounts:dashboard')
        self.assertEqual(resolve(url).func.view_class, views.UserDashboard)
