from django.test import TestCase
from django.contrib.auth import get_user_model
from accounts import models


class testCustomer(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create(
            username='mahdi',
            email='mahdi@gmail.com',
            password='mahdipasswd',
        )

    def test_custom_user(self):
        self.assertEqual(self.user.is_staff, False)


